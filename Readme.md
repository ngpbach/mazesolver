To work on the project (Windows 10)

1/Create a bitbucket account and accept invitation to join team

2/Install ChibiStudio_Preview20.7z (path C:/ChibiStudio is important)

    https://sourceforge.net/projects/chibios/files/ChibiStudio/
    
3/Install Git

    https://git-scm.com/
    
4/Clone the project (path is important)

    Navigate to C:\ChibiStudio\workspace_user\
    
    Right click and choose 'Git Bash here'
    
    Type git clone https://$(yourusername)@bitbucket.org/advmicrop/mazesolver.git
    
    For easier access refer to https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html
    
5/Import project on ChibiStudio

    File->Import->Existing Projects Into Workspace
    
6/Do a pull to make sure local stuffs are up to date, then check out the branch you are assigned to work on. This is important as the master branch should not be modified directly

    git pull
    
    git checkout -b (branch name)
    
7/When you need to save progress do a commit

    git commit -am "Something something was worked on"
    
8/Upload once you finish a feature

    git push origin (branch name)