/*
 * rbbluetooth.h
 *
 *  Created on: Mar 16, 2019
 *      Author: Phongbach
 */
/**
 * Wrapper for serial communication
 * Note: all none blocking API assume that serial driver buffer does not get full
 * It would block otherwise
 */
#ifndef RBBLUETOOTH_H
#define RBBLUETOOTH_H

/**
 * API (blocking)
 */
char rbBTGetChar(void);
char rbBTGetChar_nowait(void);
void rbBTGetMsg(uint8_t *buf, unsigned int size);
/**
 * API (none blocking)
 */
void rbBTSendChar(char c);
void rbBTSendInt(int num);
void rbBTSendFloat(float num);
void rbBTSendString(const char *str);

/**
 * API - Configure pins and start serial driver
 */
void rbBTInit(void);




#endif /* RBBLUETOOTH_H */
