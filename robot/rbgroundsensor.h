/*
 * rbgroundsensor.h
 *
 *  Created on: Mar 16, 2019
 *      Author: Phongbach
 */
/*
 *  QTR-1RC Reflectance Sensor
 *  https://www.pololu.com/product/2459
 *  Operation:
 *  - Start sensor by configure pin as output push-pull, high.
 *  - It take about 10usec for collector voltage to charge up to steady state, due to collector capacitor.
 *  - Then pin will be set as alternate function, ICU start timer immediately
 *  - ICU will call back when detect input goes low, discharge time would be the width reported by ICU.
 *  - Time for collector voltage to go from steady state high to low threshold
 *  depends on current through collector capacitor.
 *  - This current is modulated by photo transistor, proportional to the reflected infrared light.
 *  - With a strong reflectance (white surface), the decay time can be as low as several dozen microseconds;
 *  with no reflectance (black surface), the decay time can be up to a few milliseconds.
 */

#ifndef RBGROUNDSENSOR_H
#define RBGROUNDSENSOR_H

#define THICKNESS     10u            // Black line thickness determined by times seen black

struct gndSensor{
  binary_semaphore_t sem;
  bool black;
};
extern struct gndSensor gndSensor;

/**
 * API - Start ICU for timimg measurements of reflectance sensor
 */
void rbGNDsensorInit(void);

/**
 * API - Start measuring time for reflectance sensor to discharge (non blocking)
 * Set pins as output high
 * Sleep briefly to let capacitor charge fully
 * Set pin as input, ICU should capture rising edge immediately and will callback on falling edge
 */
void rbGNDsensorStart(void);

#endif /* RBGROUNDSENSOR_H */
