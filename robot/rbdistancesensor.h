/*
 * rbdistance.h
 *
 *  Created on: Mar 16, 2019
 *      Author: Phongbach
 */
/**
*  Sharp GP2Y0A41SK0F Analog Distance Sensor 4-30cm
*  https://www.pololu.com/product/2464
*/
#ifndef RBDISTANCESENSOR_H
#define RBDISTANCESENSOR_H

/**
 * API - Initialize pins mode and start chibi hal adc driver
 */
void rbDisADCInit(void);

/**
 * API - Start ADC sampling sequence (blocking)
 * Input: reference to disFront and disRight
 * Modify: disFront, disRight
 */
void rbDisGet(float *disFront, float *disRight);

#endif /* RBDISTANCESENSOR_H */
