/*
 * pinsdefinition.h
 *
 *  Created on: Mar 17, 2019
 *      Author: Phongbach
 */

/*
 * For quick change of pin assignments
 */

#ifndef RBPINDEFINITION_H
#define RBPINDEFINITION_H

/*
 * Motor pins
 * Check datasheet: Alternate function mapping
 * Need switch TRUE for PWM in halconf.h and matching PWM TIM number in mcuconf.h
 */
#define MOTOR_PWMPIN_L      PAL_LINE(GPIOE, 9U)       //Left motor PWM pin (AF1 is TIM1 channel 1)
#define MOTOR_PWMPIN_R      PAL_LINE(GPIOE, 11U)      //Right motor PWM pin (AF1 is TIM1 channel 2)
#define MOTOR_PWM_DRV       PWMD1
#define MOTOR_CHAN_LEFT     0                         //Channel 1
#define MOTOR_CHAN_RIGHT    1                         //Channel 2
#define MOTOR_DIRPIN_L      PAL_LINE(GPIOA, 4U)       //Left motor direction control pin
#define MOTOR_DIRPIN_R      PAL_LINE(GPIOA, 5U)       //Right motor direction control pin
#define MOTOR_PALMODE_L     PAL_MODE_ALTERNATE(1)
#define MOTOR_PALMODE_R     PAL_MODE_ALTERNATE(1)

/*
 * Ground reflectance sensor pins
 * Pin need the ability to switch between GPIO output mode and input capture unit mode
 * Check datasheet: Alternate function mapping
 * Need switch TRUE for ICU in halconf.h and matching ICU TIM number in mcuconf.h
 * Note: ICU driver can only use channel 1 or 2 (not both at the same time for a single TIM)
 */
#define GNDSS_PIN_L             PAL_LINE(GPIOA, 1U)     //AF2 is ICU TIM5 channel 2
//#define GNDSS_PIN_R             PAL_LINE(GPIOA, 2U)     //AF3 is ICU TIM9 channel 1
#define GNDSS_ICU_DRV_L         ICUD5
//#define GNDSS_ICU_DRV_R         ICUD9
#define GNDSS_PALMODE_L         PAL_MODE_ALTERNATE(2)
//#define GNDSS_PALMODE_R         PAL_MODE_ALTERNATE(3)
#define GNDSS_ICU_CHAN_L        ICU_CHANNEL_2
//#define GNDSS_ICU_CHAN_R        ICU_CHANNEL_1

/*
 * Distance sensor pins
 * Check datasheet: Additional function
 * Need switch TRUE for ADC in halconf.h and matching ADC number in mcuconf.h
 */
#define DISTAN_PIN_FRONT          PAL_LINE(GPIOB, 0U)     //Front distance sensor pin (ADC1_IN8)
#define DISTAN_PIN_RIGHT          PAL_LINE(GPIOB, 1U)     //Right distance sensor pin (ADC1_IN9)
#define DISTAN_ADC_DRV            ADCD1
#define DISTAN_ADC_CHAN_FRONT     ADC_CHANNEL_IN8
#define DISTAN_ADC_CHAN_RIGHT     ADC_CHANNEL_IN9
#define DISTAN_ADC_NUM_CHAN       2
#define DISTAN_ADC_SMPR_FRONT     ADC_SMPR2_SMP_AN8
#define DISTAN_ADC_SMPR_RIGHT     ADC_SMPR2_SMP_AN9


/*
 * Bluetooth pins
 * Need switch TRUE for Serial Driver in halconf.h and matching USART number for serial in mcuconf.h
 * Makefile need: include $(CHIBIOS)/os/hal/lib/streams/streams.mk
 * Need to include chprintf.h
 */
#define BLUTOO_PIN_TX       PAL_LINE(GPIOA, 2U)
#define BLUTOO_PIN_RX       PAL_LINE(GPIOA, 3U)
#define BLUTOO_SRDRV        SD2                              //USART2 (PA2_TX and PA3_RX)
#define BLUTOO_PALMODE_TX   PAL_MODE_ALTERNATE(7)
#define BLUTOO_PALMODE_RX   PAL_MODE_ALTERNATE(7)
#define BLUTOO_BAUD         115200                           //Bluesmirf default baud

#endif /* RBPINDEFINITION_H */
