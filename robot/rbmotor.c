/*
 * rbmotor.c
 *
 *  Created on: Mar 16, 2019
 *      Author: Phongbach
 */

#include "rbmotor.h"
#include "rbpindefinition.h"
#include "rbstate.h"
#include "hal.h"
#include "ch.h"

#define PWMCLKFREQ      1000000             //1MHz
#define PWMPERIOD       PWMCLKFREQ/10000    //100us

static pwmcnt_t maxPWM;     //percentage as an integer between 0 and 10000

/**
 * PWM configurations
 */
static PWMConfig pwmcfg = {
  .frequency = PWMCLKFREQ,
  .period = PWMPERIOD,
  .callback = NULL,
  .channels = { {PWM_OUTPUT_ACTIVE_HIGH, NULL},      //Channel 1
                {PWM_OUTPUT_ACTIVE_HIGH, NULL},      //Channel 2
                {PWM_OUTPUT_DISABLED, NULL},
                {PWM_OUTPUT_DISABLED, NULL} },
  .cr2 = 0,
  .dier = 0
};

/**
 * Set the PWM duty cycles
 * Internal function
 * Input: left motor duty cycles, right motor duty cycles
 * Modify: PWM duty cycles outputs
 */
static void _setSpd(float spdLm, float spdRm) {
  pwmEnableChannel(&MOTOR_PWM_DRV, MOTOR_CHAN_LEFT, PWM_PERCENTAGE_TO_WIDTH(&MOTOR_PWM_DRV, maxPWM*spdLm));
  pwmEnableChannel(&MOTOR_PWM_DRV, MOTOR_CHAN_RIGHT, PWM_PERCENTAGE_TO_WIDTH(&MOTOR_PWM_DRV, maxPWM*spdRm));
}

/**
 * Control the directions pins
 * Internal function
 * Input: left motor direction, right motor direction
 * Modify: GPIO pins logic
 */
static void _setDir(unsigned int dirLm, unsigned int dirRm) {
  palWriteLine(MOTOR_DIRPIN_L, dirLm);
  palWriteLine(MOTOR_DIRPIN_R, dirRm);
}

/**
 * API - Initialize pins mode and start chibi hal pwm driver
 */
void rbMotorInit(void) {
  maxPWM = 8000;        //Default to 80% max speed
  palSetLineMode(MOTOR_DIRPIN_L, PAL_MODE_OUTPUT_PUSHPULL);
  palSetLineMode(MOTOR_DIRPIN_R, PAL_MODE_OUTPUT_PUSHPULL);
  palSetLineMode(MOTOR_PWMPIN_L, MOTOR_PALMODE_L);
  palSetLineMode(MOTOR_PWMPIN_R, MOTOR_PALMODE_R);
  pwmStart(&MOTOR_PWM_DRV, &pwmcfg);
}

/**
 * API - Adjust max duty cycle possible
 * Input: unsigned int (20-100)
 * Output: false - invalid value; true - success
 * Modify: maxPWM
 */
bool rbMotorSpdLim(unsigned int spd) {
  if (spd < 20 || spd > 100u) return false;
  maxPWM = spd*100;
  return true;
}

/**
 * API - Call functions to change direction and power to motors
 * Input: reference to rbState object
 */
void rbMotorSet(rbstate_t const *rbStatep) {
  chMtxLock(&rbStMutex);
  _setSpd(rbStatep->spdLm, rbStatep->spdRm);
  _setDir(rbStatep->dirLm, rbStatep->dirRm);
  chMtxUnlock(&rbStMutex);
}
