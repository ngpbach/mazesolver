# List of all the board related files.
ROBOTSRC = ./robot/rbstate.c				\
		   ./robot/rbmotor.c				\
		   ./robot/rbdistancesensor.c		\
		   ./robot/rbgroundsensor.c			\
		   ./robot/rbbluetooth.c

# Required include directories
ROBOTINC = ./robot

# Shared variables
ALLCSRC += $(ROBOTSRC)
ALLINC  += $(ROBOTINC)
