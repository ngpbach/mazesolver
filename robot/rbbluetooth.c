/*
 * rbbluetooth.c
 *
 *  Created on: Mar 16, 2019
 *      Author: Phongbach
 */
/**
 * Wrapper for serial communication
 * Note: all none blocking API assume that serial driver buffer does not get full
 * It would block otherwise
 */

#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "rbbluetooth.h"
#include "rbpindefinition.h"

static BaseSequentialStream *msgStream = (BaseSequentialStream *)&BLUTOO_SRDRV;

/**
 * Serial configurations
 * Default parameters: 38400Baud, 8 bits, 1 stop bit, no flow ctrl, no parity
 */
static SerialConfig srcfg = {
                             .speed = BLUTOO_BAUD,
                             .cr1 = 0,
                             .cr2 = USART_CR2_STOP1_BITS,
                             .cr3 = 0
};

/**
 * API (blocking)
 */
char rbBTGetChar(void) {
  return (char)sdGet(&BLUTOO_SRDRV);
}

/**
 * API (blocking)
 */
char rbBTGetChar_nowait(void){
  return (char)sdGetTimeout(&BLUTOO_SRDRV, TIME_IMMEDIATE);
}

/**
 * API (blocking)
 */
void rbBTGetMsg(uint8_t *buf, unsigned int size) {
  sdRead(&BLUTOO_SRDRV, buf, size);
}

/**
 * API (none blocking)
 */
void rbBTSendChar(char c) {
  sdPut(&BLUTOO_SRDRV,(uint8_t)c);
}

/**
 * API (none blocking)
 */
void rbBTSendInt(int num) {
  chprintf(msgStream, "%d", num);
}

/**
 * API (none blocking)
 */
void rbBTSendFloat(float num) {
  int dec = (int)num;
  int frac = (int)(100*num) - 100*dec;
  if (frac < 0 ) frac = -frac;
  chprintf(msgStream, "%d.%d", dec, frac);
}

/**
 * API (none blocking)
 */
void rbBTSendString(const char *str) {
  chprintf(msgStream, str);
}

/**
 * API - Configure pins and start serial driver
 */
void rbBTInit(void) {
  palSetLineMode(BLUTOO_PIN_TX, BLUTOO_PALMODE_TX);
  palSetLineMode(BLUTOO_PIN_RX, BLUTOO_PALMODE_RX);
  sdStart(&BLUTOO_SRDRV, &srcfg);
  chprintf(msgStream, "\nBluetooth starting up...\n");
}

