/*
 * robot.h
 * Define a type rbstate_t with various data members and methods
 * Used for keeping track of robot state machine
 * No interaction with hardware
 *  Created on: Mar 15, 2019
 *      Author: Phongbach
 */

#include "ch.h"
#include "rbstate.h"

/* Braking power, no more than TURNBACK_DIS, otherwise premature stop */
#define BRAKE_FACTR 8.0f
/* Distance to wall threshold to consider "approaching wall", in cm */
#define BRAKE_DIS 13.0f
/* Distance to wall threshold to consider "reached wall", in cm */
#define TURNBACK_DIS 10.0f
/* Distance to wall threshold to consider "see right path", in cm */
#define RPATH_DIS 30.0f
/* Distance to wall threshold to consider "see right wall", in cm */
#define RWALL_DIS 12.0f
/* Reference distance to wall, in cm */
#define REFDIS 10.0f

mutex_t rbStMutex;

/**
 * pidCalc function
 * Based on current state of distances, calculate gain
 * Input: rbstate_t*
 * Output: none
 * Modify: rbStatep->gain
 */
static void _pidCalc(rbstate_t *rbStatep) {
  float err = 0, dErr = 0;
  static float iErr = 0, prevErr = 0;

  err = rbStatep->disRight - REFDIS;
  if (rbStatep->gain < 0.5f) {  // anti wind up, iErr should not accumulate to saturation
      iErr += err;
  }
  dErr = err - prevErr;
  // Gain should mostly stay less than 1 so to not saturate
  rbStatep->gain = (rbStatep->Kp)*err + (rbStatep->Ki)*iErr + (rbStatep->Kd)*dErr;
  prevErr = err;
  rbStatep->err = err;
}

/**
 * brakeCalc function
 * If robot see wall in front, it will reduce speed proportionately to avoid sudden stop
 * Input: rbstate_t*
 * Output: none
 * Modify: rbStatep->brake
 */
static void _brakeCalc(rbstate_t *rbStatep) {
  if (rbStatep->disFront < BRAKE_DIS) {
      rbStatep->brake = BRAKE_FACTR/(rbStatep->disFront);      // brake power is inversely proportional to front distance to wall
  } else {
      rbStatep->brake = 0;
  }
}

/**
 * mtorSpdCalc function
 * Calculate the proportional for each motors
 * Input: rbstate_t*
 * Output: none
 * Modify: rbStatep->spdL; rbStatep->spdR
 */
static void _mtorSpdCalc(rbstate_t *rbStatep) {
  if (rbStatep->gain >= 0) {                              // steer right
      rbStatep->spdLm = 1 - rbStatep->brake;         // Left motor max
      rbStatep->spdRm = (rbStatep->spdLm)*(1 - rbStatep->gain);         // right motor < left
  } else {                                      // steer left
      rbStatep->spdRm = 1 - rbStatep->brake;               // right motor max
      rbStatep->spdLm = (rbStatep->spdRm)*(1 + rbStatep->gain);               // Left motor < right
  }
}

/**
 * Estimate robot next state using sensors inputs
 * Input: rbstate_t*
 * Output: none
 * Modify: rbStatep->curState; rbStatep->prevState
 */
static void _stateEst(rbstate_t *rbStatep) {
  rbStatep->prevState = rbStatep->curState;
  switch (rbStatep->curState) {
  case FWD:
    if (rbStatep->disFront < TURNBACK_DIS) {
      rbStatep->curState = TURNBACK;
    } else if (rbStatep->disRight > RPATH_DIS) {
      rbStatep->curState = TURNRIGHT;
    }
    break;
  case TURNRIGHT:
    if (rbStatep->disRight < RWALL_DIS) {
      rbStatep->curState = FWD;
    }
    break;
  case TURNBACK:
    if (rbStatep->disFront > BRAKE_DIS) {
      rbStatep->curState = PAUSE;
    }
    break;
  case PAUSE:            // Add slight pause to prevent overshoot when turning
    chThdSleepMilliseconds(50);
    rbStatep->curState = FWD;
  case STOP:
    break;
  }
}

/**
 * Update motors speed and direction values
 * based on current state
 * Input: rbstate_t*
 * Output: none
 * Modify: dirLm; dirRm; spdLm; spdRm
 */
static void _mtorStateUpd(rbstate_t *rbStatep) {
  switch (rbStatep->curState) {
  case FWD:
    rbStatep->dirLm = SPINFWD;
    rbStatep->dirRm = SPINFWD;
    _pidCalc(rbStatep);
    _brakeCalc(rbStatep);
    _mtorSpdCalc(rbStatep);
    break;
  case TURNRIGHT:
    rbStatep->dirLm = SPINFWD;
    rbStatep->dirRm = SPINFWD;
    rbStatep->spdLm = 1.0;
    rbStatep->spdRm = 0.4;
    break;
  case TURNBACK:
    rbStatep->dirLm = SPINREV;
    rbStatep->dirRm = SPINFWD;
    rbStatep->spdLm = 1.0;
    rbStatep->spdRm = 1.0;
    break;
  case PAUSE:
    rbStatep->spdLm = 0;
    rbStatep->spdRm = 0;
    break;
  case STOP:
    rbStatep->spdLm = 0;
    rbStatep->spdRm = 0;
    break;
  }
}

/**
 * All rbState members that need explicit initial value need to be done here.
 */
void rbSt_Init(rbstate_t *rbStatep) {
  chMtxObjectInit(&rbStMutex);
  rbStatep->curState = STOP;
}

/**
 * Update robot distance calculated from distance sensors value
 */
void rbSt_DisUpdate(rbstate_t *rbStatep, float disFront, float disRight) {
  chMtxLock(&rbStMutex);
  rbStatep->disFront = disFront;
  rbStatep->disRight = disRight;
  chMtxUnlock(&rbStMutex);
}

/**
 * Update robot PID constants, received from commands
 * Output: false - invalid values; true - success
 */
bool rbSt_PIDtune(rbstate_t *rbStatep, float Kp, float Ki, float Kd) {
  if (Kp > 0.5 || Kp < 0 ||
      Ki > 0.5 || Ki < 0 ||
      Kd > 0.5 || Kd < 0) {
    return false;
  }
  chMtxLock(&rbStMutex);
  rbStatep->Kp = Kp;
  rbStatep->Ki = Ki;
  rbStatep->Kd = Kd;
  chMtxUnlock(&rbStMutex);
  return true;
}

/**
 * Use distance values to update various parameters of robot state accordingly
 * Input: rbstate_t*
 * Output: none
 * Modify: various rbState data, refer to nested functions call
 */
void rbSt_Update(rbstate_t *rbStatep) {
  chMtxLock(&rbStMutex);
  _stateEst(rbStatep);
  _mtorStateUpd(rbStatep);
  chMtxUnlock(&rbStMutex);
}


/**
 * State after updated will be start
 */
void rbSt_Stop(rbstate_t *rbStatep) {
  chMtxLock(&rbStMutex);
  rbStatep->curState = STOP;
  chMtxUnlock(&rbStMutex);
}

/**
 * State after updated will be forward
 */
void rbSt_Start(rbstate_t *rbStatep) {
  chMtxLock(&rbStMutex);
  rbStatep->curState = FWD;
  chMtxUnlock(&rbStMutex);
}

/**
 * Check if robot is stopped
 */
bool rbSt_isStopped(rbstate_t *rbStatep) {
  bool res;
  chMtxLock(&rbStMutex);
  res = (rbStatep->curState == STOP);
  chMtxUnlock(&rbStMutex);
  return res;
}
