/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"
#include "rbstate.h"
#include "rbmotor.h"
#include "rbgroundsensor.h"
#include "rbdistancesensor.h"
#include "rbbluetooth.h"
#include <stdlib.h> //Need $(CHIBIOS)/os/various/syscalls.c to CSRC in Makefile

static binary_semaphore_t sendlogSem;
static binary_semaphore_t blinkySem;
static binary_semaphore_t rbStSem;
static rbstate_t rbState;
static systime_t el_time;
static char menuMsg[] =
    "\n(1)Start\n"
    "(2)Enter PID parameters\n"
    "(3)Set max speed\n"
    "(4)Test Distance sensor\n"
    "(5)Test Ground sensor\n"
    "(6)Test Bluetooth\n"
    "(7)Test Motor\n"
    "(anykey)Stop\n";


/******************************************************************************
 * Blinky Thread
 * Blink when some event happens
 * Blocked when waiting for semaphores or events.
 */
static THD_WORKING_AREA(wa_Blinky, 128);
static THD_FUNCTION(blinky, arg) {
  (void)arg;
  chRegSetThreadName("blinky");
  while (true) {
    chBSemWait(&blinkySem);
    palSetPad(GPIOD, GPIOD_LED4);       //Green
    chThdSleepMilliseconds(100);
    palClearPad(GPIOD, GPIOD_LED4);     //Green
    chThdSleepMilliseconds(100);
  }
}

/******************************************************************************
 * SendLog Thread
 * Send data over bluetooth when signal to do so
 * Blocked waiting for semaphore signal data ready to be sent
 */
static THD_WORKING_AREA(wa_SendLog, 128);
static THD_FUNCTION(SendLog, arg) {
  (void)arg;
  chRegSetThreadName("SendLog");
  int count = 0;
  while (true) {
    chBSemWait(&sendlogSem);
    /* Send one values every 10 updates (100ms) */
    if (count == 10 && rbState.err < 10) {  // ignore anomaly (right path)
      rbBTSendFloat(rbState.err);
      rbBTSendChar('\n');
      chBSemSignal(&blinkySem);
      count = 0;
    } else {
      count++;
    }
  }
}


/******************************************************************************
 * Stmachine Thread
 * Task to update robot state and output to motor the new state
 * Blocked when waiting for semaphore signify new changes to robot data
 */
static THD_WORKING_AREA(wa_Stmachine, 128);
static THD_FUNCTION(Stmachine, arg){
  (void)arg;
  chRegSetThreadName("Stmachine");
  rbSt_PIDtune(&rbState, 0.25f, 0.005f, 0.4f); //Default pid parameters
  while (true) {
    chBSemWait(&rbStSem);    //Wait for new changes to robot data
    rbSt_Update(&rbState);
    rbMotorSet(&rbState);
    if (!rbSt_isStopped(&rbState)) {
      chBSemSignal(&sendlogSem);
    }
  }
}

/******************************************************************************
 * DisSensor Thread
 * Task to start ADC sequencer for the distance sensors
 * Blocked by sleeping
 * Update and post semaphore for robot state change
 */
static THD_WORKING_AREA(wa_DisSensor, 128);
static THD_FUNCTION(DisSensor, arg){
  (void)arg;
  chRegSetThreadName("DisSensor");
  float disFront, disRight;
  while (true) {
    if (rbSt_isStopped(&rbState)) {
      chThdSleepMilliseconds(100);
      continue;
    }

    rbDisGet(&disFront, &disRight);
    rbSt_DisUpdate(&rbState, disFront, disRight);
    chBSemSignal(&rbStSem);     //Signal semaphore for rbstate update
    chThdSleepMilliseconds(20);
  }
}


/******************************************************************************
 * GNDsensor Thread
 * Task to trigger reflectance sensors
 * Sleep: utilize chThdSleepUntil to make frequency more deterministic
 */
static THD_WORKING_AREA(wa_GNDsensor, 128);
static THD_FUNCTION(GNDsensor, arg){
  (void)arg;
  chRegSetThreadName("GNDsensor");
  systime_t time;
  while (true) {
    if (rbSt_isStopped(&rbState)) {
      chThdSleepMilliseconds(100);
      continue;
    }
    time = chVTGetSystemTimeX();
    rbGNDsensorStart();
    chThdSleepUntil(time + MS2RTC(CH_CFG_ST_FREQUENCY, 3));
  }
}

/**
 * GNDsensorHelper Thread
 * Blocked when waiting for semaphore from input capture callback
 * Determine the thickness of black line and update robot state accordingly
 */
static THD_WORKING_AREA(wa_GNDsensorHelper, 128);
static THD_FUNCTION(GNDsensorHelper, arg){
  (void)arg;
  chRegSetThreadName("GNDsensorHelper");
  static unsigned int count = 0;
  while (true) {
    if (rbSt_isStopped(&rbState)) {
      chThdSleepMilliseconds(100);
      continue;
    }

    chBSemWait(&gndSensor.sem);
    if (gndSensor.black == true) {
      count++;
      //rbBTSendInt(count);
      //rbBTSendString("\t");
      if (count > 2*THICKNESS) {    //double black line
        rbSt_Stop(&rbState);
        chBSemSignal(&rbStSem);     //Signal semaphore for rbstate update
        el_time = RTC2MS(CH_CFG_ST_FREQUENCY, chVTGetSystemTimeX()) - el_time;
        rbBTSendString("\nElapsed Time: ");
        rbBTSendInt(el_time);
      } else if (count > THICKNESS) {
        //TODO do something if single black line crossed
      }
    } else {            //reset count if sees white
      count = 0;
    }
  }
}

/******************************************************************************
 * Menu Thread
 * Task to display a menu when robot stopped, and to capture a key press for
 * various commands
 * Blocked when waiting for key press
 */
static THD_WORKING_AREA(wa_Menu, 128);
static THD_FUNCTION(Menu, arg){
  (void)arg;
  chRegSetThreadName("Menu");
  char charBuf;
  uint8_t msgBuf[5] = "xxxx\0";
  float disFront, disRight;
  float Kp, Ki, Kd;
  unsigned int spd;
  while (true) {
    if (rbSt_isStopped(&rbState)) {
      rbBTSendString(menuMsg);
    }
    charBuf = rbBTGetChar();    //Blocked here until getting a char
    rbSt_Stop(&rbState);
    chBSemSignal(&rbStSem);     //Signal semaphore for rbstate update
    rbBTSendString("\n----------Stopped----------\n");

    switch (charBuf) {
    case '1':   //Start
      rbBTSendString("\nStarting..............\n");
      el_time = RTC2MS(CH_CFG_ST_FREQUENCY, chVTGetSystemTimeX());
      rbSt_Start(&rbState);
      chBSemSignal(&rbStSem);   //Signal semaphore for rbstate update

      break;
    case '2': // Tune PID
      do {
        rbBTSendString("\nEnter Kp (.xxx format)\n");
        rbBTGetMsg(msgBuf,4);
        // TODO atof uses heap. Bad. Write helper and avoid using dynamic mem
        Kp = atof((char*)msgBuf);
        rbBTSendString("\nEnter Ki (.xxx format)\n");
        rbBTGetMsg(msgBuf,4);
        Ki = atof((char*)msgBuf);
        rbBTSendString("\nEnter Kd (.xxx format)\n");
        rbBTGetMsg(msgBuf,4);
        Kd = atof((char*)msgBuf);
      } while (!rbSt_PIDtune(&rbState, Kp, Ki, Kd));

      break;
    case '3':   //Set max motor speed
      do {
        rbBTSendString("\nEnter max speed value in xxxx format (0020-0100)\n");
        rbBTGetMsg(msgBuf,4);
        // TODO atoi uses heap. Bad. Write helper and avoid using dynamic mem
        spd = atoi((char*)msgBuf);
      } while (!rbMotorSpdLim(spd));

      break;
    case '4':   //Test distance sensor
      while (rbBTGetChar_nowait() != ' ') {
        rbDisGet(&disFront, &disRight);
        rbBTSendFloat(disFront);
        rbBTSendChar('\t');
        rbBTSendFloat(disRight);
        chThdSleepSeconds(1);
        rbBTSendString("\nPress space to stop\n");
      }

      break;
    case '5':   //Test ground sensor
      while (rbBTGetChar_nowait() != ' ') {
        rbGNDsensorStart();
        chThdSleepSeconds(1);
        if (gndSensor.black == true) {
          rbBTSendString("Black");
        } else {
          rbBTSendString("White");
        }
        rbBTSendString("\nPress space to stop\n");
      }

      break;
    case '6':   //Test Bluetooth
      rbBTSendString("\nPress a key\n");
      charBuf = rbBTGetChar();
      rbBTSendString("\nYou pressed: ");
      rbBTSendChar(charBuf);
      chThdSleepSeconds(1);

      break;
    case '7':   //Test motor
      /*********************************
       * Sub menu 7
       */
      rbBTSendString("\nTry pressing 1 or 2 or 3\n");
      charBuf = rbBTGetChar();    //Blocked here until getting a char
      switch (charBuf) {
      case '1':
        rbState.spdLm = 1;
        rbState.spdRm = 1;
        rbState.dirLm = SPINFWD;
        rbState.dirRm = SPINFWD;
        rbMotorSet(&rbState);

        break;
      case '2':
        rbState.spdLm = 1;
        rbState.spdRm = 1;
        rbState.dirLm = SPINREV;
        rbState.dirRm = SPINFWD;
        rbMotorSet(&rbState);

        break;
      case '3':
        rbState.spdLm = 1;
        rbState.spdRm = 1;
        rbState.dirLm = SPINFWD;
        rbState.dirRm = SPINREV;
        rbMotorSet(&rbState);

        break;
       /* End sub menu 7
       *********************************/
      }
    default :   //Stop
      break;
    }
  }
}

/******************************************************************************
 * Application entry point.
 */
int main(void) {
  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
  */
  halInit();
  chSysInit();
  /* Various robot drivers initializations. */
  chBSemObjectInit(&rbStSem, true);
  chBSemObjectInit(&sendlogSem, true);
  chBSemObjectInit(&blinkySem, true);
  rbSt_Init(&rbState);
  rbMotorInit();
  rbBTInit();
  rbGNDsensorInit();
  rbDisADCInit();
  /* Spawn threads. */
  chThdCreateStatic(wa_Blinky, sizeof(wa_Blinky),
                    NORMALPRIO-1, blinky, NULL);
  chThdCreateStatic(wa_SendLog, sizeof(wa_SendLog),
                    NORMALPRIO-1, SendLog, NULL);
  chThdCreateStatic(wa_Stmachine, sizeof(wa_Stmachine),
                    NORMALPRIO+1, Stmachine, NULL);
  chThdCreateStatic(wa_DisSensor, sizeof(wa_DisSensor),
                    NORMALPRIO+2, DisSensor, NULL);
  chThdCreateStatic(wa_GNDsensor, sizeof(wa_GNDsensor),
                    NORMALPRIO+2, GNDsensor, NULL);
  chThdCreateStatic(wa_GNDsensorHelper, sizeof(wa_GNDsensorHelper),
                    NORMALPRIO+2, GNDsensorHelper, NULL);
  chThdCreateStatic(wa_Menu, sizeof(wa_Menu), NORMALPRIO-1,
                    Menu, NULL);
  /* Heart beat */
  while (true) {
    palSetPad(GPIOD, GPIOD_LED3);       //Orange
    chThdSleepMilliseconds(500);
    palClearPad(GPIOD, GPIOD_LED3);     //Orange
    chThdSleepMilliseconds(500);
  }
}
